# instantTranslate #

* autores: Alexy Sadovoy, ruslan, Beqa Gozalishvili e outros colaboradores
  do NVDA.
* baixe a [versão 2.2beta2][1]

Este complemento serve para traduzir um texto selecionado e/ou da área de
transferência dum idioma para outro.  Isso é feito por meio do serviço
Google Translate.

## Configurando idiomas ##

Para configurar os idiomas de origem e destino, a partir do menu do NVDA vá
para Preferências, daí para Opções do Tradutor Instantâneo.  Existem duas
caixas de combinação denominadas "traduzir de" e "traduzir para".  Selecione
os idiomas e pressione ENTER no botão OK.

## Como usar este complemento ##

Há duas formas de usar o complemento:

1. Selecione um texto usando comandos de seleção (shift com setas, por
   exemplo). Aí pressione Shift+NVDA+T para traduzir o texto
   selecionado. Será então lido o texto traduzido, contanto que o
   sintetizador em uso suporte o idioma de destino.
2. Copie um texto para a área de transferência, aí pressione Shift+NVDA+Y
   para traduzi-lo ao idioma destino.

## Mudanças na 2.2 ##
* Aumentado o número de caracteres para 1500.
* Adicionada a tecla de atalho t para o item de menu Opções do Tradutor
  Instantâneo
* Adicionada uma caixa de seleção para configurar a cópia de resultados de
  traduções.
* Armazena arquivo de configuração na raiz da pasta de opções.
* Novos idiomas: Alemão, Árabe, Aragonês, Coreano, Croata, Eslovaco,
  Esloveno, Espanhol, Finlandês, Francês, Galego, Holandês, Húngaro,
  Italiano, Japonês, Nepalês, Polonês, Português do Brasil, Tâmil, Turco.

## Mudanças na 2.1 ##
* Agora o complemento pode traduzir o texto da área de transferência ao
  pressionar NVDA+SHIFT+y.

## Mudanças na 2.0 ##
* Adicionado configurador gráfico onde pode escolher os idiomas fonte e
  destino.
* Adicionado item de menu do complemento, encontrado no menu Preferências.
* As configurações agora são escritas num arquivo de configuração separado.
* Agora os resultados de traduções são colocados automaticamente na área de
  transferência para futuras manipulações.

## Mudanças na 1.0 ##
* Versão inicial.

[[!tag dev]]

[1]: http://addons.nvda-project.org/files/get.php?file=it
