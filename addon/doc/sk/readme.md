# instantTranslate #

* Autori: Alexey Sadovoy, ruslan, Beqa Gozalishvili a ďalší.
* Stiahnuť [verzia 2.2beta2][1]

pomocou tohto doplnku môžete prekladať vybratý text, alebo text umiestnený v
schránke pomocou služby Prekladač Google.

## nastavenie jazykov ##

Aby ste nastavili jazyk, z ktorého a do ktorého budete prekladať, vstúpte do
menu NVDA>možnosti>Nastavenia Instant translate. Tu sú dva zoznamové rámiky,
označené ako "z jazyka"  a "do jazyka". Nastavte požadované jazyky a stlačte
OK.

## Ako prekladať ##

Máte dve možnosti:

1. Vyberte nejaký text (napríklad pomocou shift+šípky). Potom stlačte
   nvda+shift+t. NVDA prečíta preložený text, ak obsahuje znaky, ktoré
   potporuje práve používaný hlasový výstup.
2. Skopírujte text do schránky a stlačte nvda+shift+y.

## Zmeny vo verzii 2.2 ##
* Počet znakov rozšírený na 1500
* pridaná skratka pre nastavenia Instant translate do názvu položky v menu
* pridané začiarkavacie políčko na nastavenie kopírovania prekladu.
* Konfiguračný súbor je v hlavnom adresáry s nastaveniami.
* Nové preklady: Aragónčina, arabčina, brazílska portugalčina, chorvátčina,
  holandčina, fínčina, francúzština, galícijčina, nemčina, maďarčina,
  taliančina, japončina, kórejčina, nepálčina, poľština, slovenčina,
  slovinčina, španielčina, tamilčina, turečtina.

## Zmeny vo verzii 2.1 ##
* NVDA+shift+y preloží text v schránke.

## Zmeny vo verzii 2.0 ##
* Pridané okno kde sa dá nastaviť cieľový a koncový jazyk.
* Dialóg sa dá otvoriť z menu možnosti.
* nastavenia sa ukladajú do samostatného súboru.
* Výsledok prekladu sa automaticky skopíruje do schránky.

## Zmeny vo verzii 1.0 ##
* Prvé vydanie.

[[!tag dev]]

[1]: http://addons.nvda-project.org/files/get.php?file=it
