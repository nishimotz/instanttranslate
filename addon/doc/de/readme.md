# Sofortübersetzung #

* Authoren: Alexy Sadovoy, ruslan, Beqa Gozalishvili und andere
  nvda-Entwickler.
* download [Version 2.2][1]

Diese Erweiterung wird verwendet, um markierten Text bzw. Text er
Zwischenablage in eine andere Sprache zu übersetzen.

## Sprachen einstellen ##

Um die Quell- und Zielsprache einzustellen, gehen Sie in das NVDA-Menü und
wählen dort Einstellungen -> Einstellungen für die Sofortübersetzung. Dort
finden Sie zwei Kombinationsfelder namens Ausgangssprache und
Zielsprache. Nehmen Sie die gewünschten Sprachen vor und drücken Sie Eingabe
auf dem OK-Schalter.

## Anwenden der Erweiterung ##

Es gibt 2 Möglichkeiten, diese Erweiterung zu verwenden:

1. Markieren Sie den Text mit den Befehlen zum markieren von text
   (z. B. Umschalt+Pfeiltasten). Drücken Sie anschließend Umschalt+NVDA+t,
   um den markierten Text übersetzen zu lassen. Der übersetzte Text wird
   dann vorgelesen, vorrausgesetzt, dass die Sprachausgabe den Text
   unterstützt.
2. Kopieren Sie einen Text in die Zwischenablage und drücken Sie
   anschließend Umschalt+NvDA+Y, um den Text, der sich in der Zwischenablage
   befindet, in die eingestellte Sprache zu übersetzen.

## Änderungen für 2.2 ##
* Es können nun 1500 Zeichen verarbeitet werden.
* l
* Option hinzugefügt, um festzulegen, ob das Resultat der Übersetzung
  kopiert werden soll.
* Konfigurationsdatei wird im Einstellungsverzeichnis gespeichert.
* Neue Sprachen: aragonesisch, arabisch, brasilianisches portugisisch,
  kroatisch, niederländisch, Finnisch, Französisch, Galizisch, Deutsch,
  Ungarisch, Italienisch, japanisch, Koreanisch, nepalesisch, polnisch,
  Slovakisch, slovenisch, Spanisch, Tamil, Türkisch.

## Änderungen für 2.1 ##
* Die Erweiterung übersetzt nun den Text aus der Zwischenablage mittels
  nvda+y

## Änderungen für 2.0 ##
* Einstellungsdialog zur Wahl der Ein- und Ausgabesprache hinzugefügt.
* Menü für die Erweiterung im Einstellungen-Menü hinzugefügt.
* Einstellungen werden in eine separate Datei geschrieben.
* Übersetzungsergebnisse  werden nun automatisch in die Zwischenablage
  kopiert, um diese weiter verwenden zu können.

## Änderungen für 1.0 ##
* Erstveröffentlichung

[[!tag dev]]

[1]: http://addons.nvda-project.org/files/get.php?file=it
