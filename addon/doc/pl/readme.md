# Błyskawiczny tłumacz tekstu / instantTranslate #

* Autorzy: Alexy Sadovoy, ruslan, Beqa Gozalishvili i inni.
* Pobierz [wersja 2.2beta2][1]

Ta wtyczka tłumaczy tekst ze schowka lub zaznaczenia z jednego języka na
drugi. W tym celu używany jest tłumacz google.

## konfigurowanie języków ##

Aby ustawić źródłowy i docelowy język, z menu ustawień NVDA wybierz pozycję
instant translate. W oknie są listy z wyborem języka źródłowego i
docelowego. Ustaw się kursorem na językach które wybierasz i wciśnij ok.

## jak użyć tego dodatku? ##

Są na to dwa sposoby

1. Zaznacz jakiś tekst używając poleceń zaznaczania (np. shift z klawiszami
   strzałek). Następnie naciśnij Shift+NvDA+T aby przetłumaczyć zaznaczony
   tekst. Przetłumaczony tekst zostanie odczytany, pod warunkiem, że
   syntezator, którego używasz, obsługuje docelowy język.
2. skopiuj tekst źródłowy do schowka i naciśnij nvda+shift+y aby go
   przetłumaczyć na docelowy język.

## zmiany dla wersji 2.2 ##
* liczba znaków w tekście zwiększona do 1500.
* skrut i pozwala szybko dostać się do ustawień wtyczki z menu nvda.
* Dodane pole wyboru ustawiające kopiowanie do schowka wyniku tłumaczenia.
* plik konfiguracyjny trzymany jest teraz w głównym katalogu ustawień nvda.
* Nowe języki: aragoński, arabski, brazylijski portugalski, chorwacki,
  holenderski, fiński, francuski, galicyjski, niemiecki, węgierski, włoski,
  japoński, koreański, nepalski, polski, słowacki, słoweński, hiszpański,
  tamilski, turecki.

## Zmiany dla wersji 2.1 ##
* Skrót klawiszowy NVDA+shift+y tłumaczy tekst w schowku.

## zmiany dla wersji 2.0 ##
* dodano okienko, w którym możemy wybrać źródłowy i docelowy język.
* dodano pozycję dla wtyczki w menu NVDA Ustawienia.
* ustawienia zapisywane są w oddzielnym pliku konfiguracyjnym.
* wyniki tłumaczenia automatycznie kopiowane są do schowka.

## zmiany dla wersji 1.0 ##
* Wstępne wydanie.

[[!tag dev]]

[1]: http://addons.nvda-project.org/files/get.php?file=it
