# Pikakääntäjä #

* Tekijät: Alexy Sadovoy, Ruslan, Beqa Gozalishvili sekä muut.
* Lataa [versio 2.2 beeta 2][1]

Tätä lisäosaa käytetään valitun ja/tai leikepöydällä olevan tekstin
kääntämiseen kielestä toiselle.  Käännös suoritetaan
Google-kääntäjä-palvelulla.

## Kielten määrittäminen ##

Lähde- ja kohdekieli määritetään menemällä NVDA-valikkoon ja valitsemalla
Asetukset-alavalikosta "Pikakääntäjä..."-vaihtoehto.  Avautuvassa
valintaikkunassa on kaksi yhdistelmäruutua, jotka ovat "Lähdekieli" ja
"Kohdekieli".  Valitse haluamasi kielet ja paina Enteriä OK-painikkeen
kohdalla.

## Miten tätä lisäosaa käytetään ##

Käyttötapoja on kaksi:

1. Valitse tekstiä valitsemiskomennoilla (esim. Shift+nuolinäppäimet). Paina
   sitten Shift+NVDA+T kääntääksesi sen. Tämän jälkeen käännetty teksti
   luetaan (edellyttäen että käyttämäsi syntetisaattori tukee kohdekieltä).
2. Kopioi tekstiä leikepöydälle. Paina sitten Shift+NVDA+Y kääntääksesi sen
   kohdekielelle.

## Muutokset versiossa 2.2 ##
* Tekstin enimmäispituutta kasvatettu 1500 merkkiin.
* Lisätty t-pikanäppäin englanninkieliseen Pikakäännöksen asetukset
  -valikkokohteeseen
* Lisätty valintaruutu käännöstuloksen kopioinnin määrittämiseksi.
* Asetustiedosto tallennetaan asetuskansion juureen.
* Uusia kieliä: arabia, aragonia, brasilianportugali, espanja, galego,
  hollanti, italia, japani, korea, kroatia, nepali, puola, ranska, saksa,
  slovakki, sloveeni, suomi, tamili, tshekki, turkki, unkari.

## Muutokset versiossa 2.1 ##
* Lisäosa voi kääntää leikepöydällä olevan tekstin painettaessa
  NVDA+Shift+Y.

## Muutokset versiossa 2.0 ##
* Lisätty graafinen käyttöliittymä, josta voidaan valita lähde- ja
  kohdekieli.
* Lisätty valikko, joka löytyy Asetukset-valikosta.
* Asetukset kirjoitetaan nyt erilliseen asetustiedostoon.
* Käännöksen tulokset kopioidaan nyt automaattisesti leikepöydälle tulevaa
  käsittelyä varten.

## Muutokset versiossa 1.0 ##
* Ensimmäinen versio.

[[!tag dev]]

[1]: http://addons.nvda-project.org/files/get.php?file=it
